JSONLoader = ( function () {

    function LegacyJSONLoader( manager ) {

        if ( typeof manager === 'boolean' ) {

            console.warn( 'THREE.JSONLoader: showStatus parameter has been removed from constructor.' );
            manager = undefined;

        }

        this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

        this.withCredentials = false;

    }

    Object.assign( LegacyJSONLoader.prototype, {

        crossOrigin: 'anonymous',

        load: function ( json, onLoad, onProgress, onError ) {

            let scope = this;

            /*
            let metadata = json.metadata;

            if ( metadata !== undefined ) {

                let type = metadata.type;

                if ( type !== undefined ) {

                    if ( type.toLowerCase() === 'object' ) {

                        console.error( 'THREE.JSONLoader: ' + url + ' should be loaded with THREE.ObjectLoader instead.' );
                        return;

                    }

                }

            }
            */

            scope.parse(json);
            //onLoad( baseballSpin3D.finalize(object.geometry));
        },

        parse: ( function () {

            function parseModel( json, geometry ) {
                console.log("Parsing Model");

                function isBitSet( value, position ) {

                    return value & ( 1 << position );

                }

                console.log("setting vars");

                let fi,

                    offset, zLength,

                    colorIndex, normalIndex, uvIndex, materialIndex,

                    type,
                    isQuad,
                    hasMaterial,
                    hasFaceVertexUv,
                    hasFaceNormal, hasFaceVertexNormal,
                    hasFaceColor, hasFaceVertexColor,

                    vertex, face, faceA, faceB, hex, normal,

                    uvLayer, uv, u, v,
                    nUvLayers = 0;

                    const faces = json.geometries.data.faces,
                    vertices = json.geometries.data.vertices,
                    normals = json.geometries.data.normals,
                    colors = json.geometries.data.colors,

                    scale = new THREE.Vector3(1,1,1);

                if ( json.geometries.data.uvs !== undefined ) {

                    // disregard empty arrays

                    for ( let i = 0; i < json.geometries.data.uvs.length; i ++ ) {

                        if ( json.geometries.data.uvs[ i ].length ) nUvLayers ++;

                    }

                    for ( let i = 0; i < nUvLayers; i ++ ) {

                        geometry.faceVertexUvs[ i ] = [];

                    }

                }

                offset = 0;
                zLength = vertices.length;

                while ( offset < zLength ) {

                    vertex = new THREE.Vector3();

                    vertex.x = vertices[ offset ++ ] * 10;
                    vertex.y = vertices[ offset ++ ] * 10;
                    vertex.z = vertices[ offset ++ ] * 10;

                    geometry.vertices.push( vertex );
                    //console.log("offset: " + offset + ", vertex:");
                    //console.log("vertex:");
                    //console.log(vertex);
                }

                offset = 0;
                zLength = faces.length;

                while ( offset < zLength ) {

                    type = faces[ offset ++ ];

                    isQuad = isBitSet( type, 0 );
                    hasMaterial = isBitSet( type, 1 );
                    hasFaceVertexUv = isBitSet( type, 3 );
                    hasFaceNormal = isBitSet( type, 4 );
                    hasFaceVertexNormal = isBitSet( type, 5 );
                    hasFaceColor = isBitSet( type, 6 );
                    hasFaceVertexColor = isBitSet( type, 7 );

                     //console.log("type", type, "bits", isQuad, hasMaterial, hasFaceVertexUv, hasFaceNormal, hasFaceVertexNormal, hasFaceColor, hasFaceVertexColor);

                    if ( isQuad ) {

                        faceA = new THREE.Face3();
                        faceA.a = faces[ offset ];
                        faceA.b = faces[ offset + 1 ];
                        faceA.c = faces[ offset + 3 ];

                        faceB = new THREE.Face3();
                        faceB.a = faces[ offset + 1 ];
                        faceB.b = faces[ offset + 2 ];
                        faceB.c = faces[ offset + 3 ];

                        offset += 4;

                        if ( hasMaterial ) {

                            materialIndex = faces[ offset ++ ];
                            faceA.materialIndex = materialIndex;
                            faceB.materialIndex = materialIndex;

                        }

                        // to get face <=> uv index correspondence

                        fi = geometry.faces.length;

                        if ( hasFaceVertexUv ) {

                            for ( let i = 0; i < nUvLayers; i ++ ) {

                                uvLayer = json.geometries.data.uvs[ i ];

                                geometry.faceVertexUvs[ i ][ fi ] = [];
                                geometry.faceVertexUvs[ i ][ fi + 1 ] = [];

                                for ( let j = 0; j < 4; j ++ ) {

                                    uvIndex = faces[ offset ++ ];

                                    u = uvLayer[ uvIndex * 2 ];
                                    v = uvLayer[ uvIndex * 2 + 1 ];

                                    uv = new THREE.Vector2( u, v );

                                    if ( j !== 2 ) geometry.faceVertexUvs[ i ][ fi ].push( uv );
                                    if ( j !== 0 ) geometry.faceVertexUvs[ i ][ fi + 1 ].push( uv );

                                }

                            }

                        }

                        if ( hasFaceNormal ) {

                            normalIndex = faces[ offset ++ ] * 3;

                            faceA.normal.set(
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ]
                            );

                            faceB.normal.copy( faceA.normal );

                        }

                        if ( hasFaceVertexNormal ) {

                            for ( let i = 0; i < 4; i ++ ) {

                                normalIndex = faces[ offset ++ ] * 3;

                                normal = new THREE.Vector3(
                                    normals[ normalIndex ++ ],
                                    normals[ normalIndex ++ ],
                                    normals[ normalIndex ]
                                );


                                if ( i !== 2 ) faceA.vertexNormals.push( normal );
                                if ( i !== 0 ) faceB.vertexNormals.push( normal );

                            }

                        }


                        faceA.color.setHex( '#000000' );
                        faceB.color.setHex( '#000000' );


                        if ( hasFaceVertexColor ) {

                            for ( let i = 0; i < 4; i ++ ) {

                                if ( i !== 2 ) faceA.vertexColors.push( new THREE.Color( '#000000' ) );
                                if ( i !== 0 ) faceB.vertexColors.push( new THREE.Color( '#000000' ) );

                            }

                        }

                        geometry.faces.push( faceA );
                        geometry.faces.push( faceB );

                    } else {

                        face = new THREE.Face3();
                        face.a = faces[ offset ++ ];
                        face.b = faces[ offset ++ ];
                        face.c = faces[ offset ++ ];

                        if ( hasMaterial ) {

                            materialIndex = faces[ offset ++ ];
                            face.materialIndex = materialIndex;

                        }

                        // to get face <=> uv index correspondence

                        fi = geometry.faces.length;

                        if ( hasFaceVertexUv ) {

                            for ( let i = 0; i < nUvLayers; i ++ ) {

                                uvLayer = json.geometries.data.uvs[ i ];

                                geometry.faceVertexUvs[ i ][ fi ] = [];

                                for ( let j = 0; j < 3; j ++ ) {

                                    uvIndex = faces[ offset ++ ];

                                    u = uvLayer[ uvIndex * 2 ];
                                    v = uvLayer[ uvIndex * 2 + 1 ];

                                    uv = new THREE.Vector2( u, v );

                                    geometry.faceVertexUvs[ i ][ fi ].push( uv );

                                }

                            }

                        }

                        if ( hasFaceNormal ) {

                            normalIndex = faces[ offset ++ ] * 3;

                            face.normal.set(
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ++ ],
                                normals[ normalIndex ]
                            );

                        }

                        if ( hasFaceVertexNormal ) {

                            for ( let i = 0; i < 3; i ++ ) {

                                normalIndex = faces[ offset ++ ] * 3;

                                normal = new THREE.Vector3(
                                    normals[ normalIndex ++ ],
                                    normals[ normalIndex ++ ],
                                    normals[ normalIndex ]
                                );

                                face.vertexNormals.push( normal );

                            }

                        }

                        face.color.setHex( '#000000' );


                        for ( let i = 0; i < 3; i ++ ) {

                            face.vertexColors.push( new THREE.Color( '#000000' ) );

                        }

                        geometry.faces.push( face );

                    }

                }

            }

            function parseSkin( json, geometry ) {

                const influencesPerVertex = ( json.geometries.data.influencesPerVertex !== undefined ) ? json.geometries.data.influencesPerVertex : 2;

                if ( json.geometries.data.skinWeights ) {

                    for ( let i = 0, l = json.geometries.data.skinWeights.length; i < l; i += influencesPerVertex ) {

                        let x = json.geometries.data.skinWeights[ i ];
                        let y = ( influencesPerVertex > 1 ) ? json.geometries.data.skinWeights[ i + 1 ] : 0;
                        let z = ( influencesPerVertex > 2 ) ? json.geometries.data.skinWeights[ i + 2 ] : 0;
                        let w = ( influencesPerVertex > 3 ) ? json.geometries.data.skinWeights[ i + 3 ] : 0;

                        geometry.skinWeights.push( new THREE.Vector4( x, y, z, w ) );

                    }

                }

                if ( json.geometries.data.skinIndices ) {

                    for ( let i = 0, l = json.geometries.data.skinIndices.length; i < l; i += influencesPerVertex ) {

                        let a = json.geometries.data.skinIndices[ i ];
                        let b = ( influencesPerVertex > 1 ) ? json.geometries.data.skinIndices[ i + 1 ] : 0;
                        let c = ( influencesPerVertex > 2 ) ? json.geometries.data.skinIndices[ i + 2 ] : 0;
                        let d = ( influencesPerVertex > 3 ) ? json.geometries.data.skinIndices[ i + 3 ] : 0;

                        geometry.skinIndices.push( new THREE.Vector4( a, b, c, d ) );

                    }

                }

                geometry.bones = json.geometries.data.bones;

                if ( geometry.bones && geometry.bones.length > 0 && ( geometry.skinWeights.length !== geometry.skinIndices.length || geometry.skinIndices.length !== geometry.vertices.length ) ) {

                    console.warn( 'When skinning, number of vertices (' + geometry.vertices.length + '), skinIndices (' +
                        geometry.skinIndices.length + '), and skinWeights (' + geometry.skinWeights.length + ') should match.' );

                }

            }

            function parseMorphing( json, geometry ) {

                let scale = json.scale;

                if ( json.geometries.data.morphTargets !== undefined ) {

                    for ( let i = 0, l = json.geometries.data.morphTargets.length; i < l; i ++ ) {

                        geometry.morphTargets[ i ] = {};
                        geometry.morphTargets[ i ].name = json.geometries.data.morphTargets[ i ].name;
                        geometry.morphTargets[ i ].vertices = [];

                        let dstVertices = geometry.morphTargets[ i ].vertices;
                        let srcVertices = json.geometries.data.morphTargets[ i ].vertices;

                        for ( let v = 0, vl = srcVertices.length; v < vl; v += 3 ) {

                            let vertex = new THREE.Vector3();
                            vertex.x = srcVertices[ v ] * scale;
                            vertex.y = srcVertices[ v + 1 ] * scale;
                            vertex.z = srcVertices[ v + 2 ] * scale;

                            dstVertices.push( vertex );

                        }

                    }

                }

                if ( json.geometries.data.morphColors !== undefined && json.geometries.data.morphColors.length > 0 ) {

                    console.warn( 'THREE.JSONLoader: "morphColors" no longer supported. Using them as face colors.' );

                    let faces = geometry.faces;
                    let morphColors = json.geometries.data.morphColors[ 0 ].colors;

                    for ( let i = 0, l = faces.length; i < l; i ++ ) {

                        faces[ i ].color.fromArray( morphColors, i * 3 );

                    }

                }

            }

            function parseAnimations( json, geometry ) {

                let outputAnimations = [];

                // parse old style Bone/Hierarchy animations
                let animations = [];

                if ( json.geometries.data.animation !== undefined ) {

                    animations.push( json.geometries.data.animation );

                }

                if ( json.geometries.data.animations !== undefined ) {

                    if ( json.geometries.data.animations.length ) {

                        animations = animations.concat( json.geometries.data.animations );

                    } else {

                        animations.push( json.geometries.data.animations );

                    }

                }

                for ( let i = 0; i < animations.length; i ++ ) {

                    let clip = THREE.AnimationClip.parseAnimation( animations[ i ], geometry.bones );
                    if ( clip ) outputAnimations.push( clip );

                }

                // parse implicit morph animations
                if ( geometry.morphTargets ) {

                    // TODO: Figure out what an appropraite FPS is for morph target animations -- defaulting to 10, but really it is completely arbitrary.
                    let morphAnimationClips = THREE.AnimationClip.CreateClipsFromMorphTargetSequences( geometry.morphTargets, 10 );
                    outputAnimations = outputAnimations.concat( morphAnimationClips );

                }

                if ( outputAnimations.length > 0 ) geometry.animations = outputAnimations;

            }

            return async function parse(json) {

                var geometry = new THREE.Geometry();

                parseModel( json, geometry );
                parseSkin( json, geometry );
                parseMorphing( json, geometry );
                parseAnimations( json, geometry );
                baseballSpin3D.computeVertices(geometry);
                geometry.computeBoundingSphere();
                geometry.computeFaceNormals();
                geometry.computeVertexNormals();
                var bufferGeometry = new THREE.BufferGeometry().fromGeometry( geometry );
                geometry.computeBoundingSphere();
                geometry.computeFaceNormals();
                geometry.computeVertexNormals();
                if(baseballSpin3D.loadingLeather){
                    baseballSpin3D.finalizeLeather(bufferGeometry);
                } else if(baseballSpin3D.loadingStitches){
                    baseballSpin3D.finalizeStitches(bufferGeometry);
                } else if(baseballSpin3D.loadingSeam){
                    baseballSpin3D.finalizeSeam(bufferGeometry);
                }


                /*
                if ( json.materials === undefined || json.materials.length === 0 ) {

                    return { geometry: geometry };

                } else {

                    let materials = json.materials;

                    return { geometry: geometry, materials: materials };

                }
                */

            };

        } )()

    } );

    return LegacyJSONLoader;

} )();