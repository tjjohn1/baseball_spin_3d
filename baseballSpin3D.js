const baseballSpin3D = {

    controlsTarget: [],
    animationFrame: {},
    scene: {},
    renderer:{},
    parentCamera: {},
    turnTableCamera: {},
    controls: {},
    container: {},
    raycaster: new THREE.Raycaster(),
    mouse: {},
    initialClean: true,
    animating: false,
    rect:{},
    objects:{},
    intersects:[],
    timeout:undefined,
    isCleaning:false,
    moving:false,
    baseballGroup:new THREE.Group(),
    loadingLeather:false,
    loadingStitches:false,
    loadingSeam:false,
    position: new THREE.Vector3(0,0,0),
    isDragging: false,
    previousMousePosition: {
        x: 0,
        y:0
    },
    defaultVector: new THREE.Vector3(1, 0, 0),
    baseballObjects:[],
    pitchDefaultVector:{},
    pitchAxes:{
        "FA": [new THREE.Vector3(1, 0, 0), new THREE.Vector3(0, 1, 0)],
        "FT": [new THREE.Vector3(1, 0, 0)],
        "SI": [new THREE.Vector3(1, 0, 0)],
        "CH": [new THREE.Vector3(1, 0, 0)],
        "FS": [new THREE.Vector3(1, 0, 0)], // splitter??
        "FC": [new THREE.Vector3(1, 0, 0)],
        "CB": [new THREE.Vector3(1, 0, 0)],
        "CU": [new THREE.Vector3(1, 0, 0)],
        "SL": [new THREE.Vector3(1, 0, 0)],
        "KC": [new THREE.Vector3(1, 0, 0)],//check on Knuckle-Curve color assignment
        "KN": [new THREE.Vector3(1, 0, 0)],
        "EP": [new THREE.Vector3(1, 0, 0)],//check on Eephus color assignment
        "SC": [new THREE.Vector3(1, 0, 0)],//check on Screwball color assignment
        "FO": [new THREE.Vector3(1, 0, 0)],
    },
    pitchSpeedDirection:{
        "FA": [.05,.05],
        "FT": [.05],
        "SI": [.05],
        "CH": [.05],
        "FS": [.05], // splitter??
        "FC": [.05],
        "CB": [.05],
        "CU": [.05],
        "SL": [.05],
        "KC": [.05],//check on Knuckle-Curve color assignment
        "KN": [.05],
        "EP": [.05],//check on Eephus color assignment
        "SC": [.05],//check on Screwball color assignment
        "FO": [.05],
    },
    currentRotationVector:[],
    currentRotationDirection:[],
    spinEfficiency:1.00,
    selectedPitchType:"FA",

    init: async function(){
        baseballSpin3D.container = $('<div id="containerDiv" style="baseballSpin3D.position:relative;display:block;"></div>');
        baseballSpin3D.container.appendTo($('body'));

        // scene
        baseballSpin3D.scene = new THREE.Scene();

        //renderer view scaling
        let lessHeight = Number(window.innerHeight*.03);
        let newHeight = Number(window.innerHeight - lessHeight);
        let lessWidth = Number(window.innerWidth*.02);
        let newWidth = Number(window.innerWidth - lessWidth);

        //renderer setup
        baseballSpin3D.renderer = new THREE.WebGLRenderer({alpha: true, antialias: true});
        baseballSpin3D.renderer.setSize(newWidth, newHeight);
        baseballSpin3D.renderer.localClippingEnabled = true;
        //renderer.physicallyCorrectLights = true;
        baseballSpin3D.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        baseballSpin3D.renderer.setPixelRatio(window.devicePixelRatio);
        baseballSpin3D.renderer.setSize(newWidth, newHeight);
        baseballSpin3D.container.append(baseballSpin3D.renderer.domElement);

        // mouse
        baseballSpin3D.mouse = new THREE.Vector2();


        // cameras

        baseballSpin3D.turnTableCamera = new THREE.PerspectiveCamera(1, window.innerWidth / window.innerHeight, -1, 5);
        baseballSpin3D.scene.add(baseballSpin3D.turnTableCamera); //required, since camera has a child light


        // background color
        baseballSpin3D.scene.background = new THREE.Color(0xDDD3D1);

        // ambient
        //baseballSpin3D.scene.add(new THREE.AmbientLight(0x404040, 1));

        // light
        let light = new THREE.PointLight(0xffffff, 1, 0, 2);
        light.position.set(500, 500, 500);
        light.shadow.mapSize.width = 1024; // default is 512
        light.shadow.mapSize.height = 1024; // default is 512
        baseballSpin3D.turnTableCamera.add(light);


        baseballSpin3D.loadLeather();



        /*
        baseballSpin3D.processObjects(baseballJSON['leather'], material).then(mesh => {
            baseballSpin3D.finalize(mesh);
        });
        */
    },

    loadLeather: function(){
        baseballSpin3D.loadingLeather = true;
        new JSONLoader().load(baseballJSON['leather']);
    },

    finalizeLeather: function(geometry){
        let material = new THREE.MeshLambertMaterial({
            color: new THREE.Color(0xF5F0EE),
            side: THREE.DoubleSide,
            transparent: false,
            reflectivity:0.1
        });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.overdraw = true;
        console.log(mesh);
        baseballSpin3D.baseballGroup.add(mesh);
        baseballSpin3D.loadingLeather = false;
        baseballSpin3D.loadStiches();
    },

    loadStiches: function(){
        baseballSpin3D.loadingStitches = true;
        new JSONLoader().load(baseballJSON['stitches']);
    },

    finalizeStitches: function(geometry){
        let material = new THREE.MeshLambertMaterial({
            color: new THREE.Color(0xC10E0E),
            side: THREE.DoubleSide,
            transparent: false,
            reflectivity:0.1
        });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.overdraw = true;
        console.log(mesh);
        baseballSpin3D.baseballGroup.add(mesh);
        mesh.position.copy(baseballSpin3D.position);
        baseballSpin3D.loadingStitches = false;
        baseballSpin3D.loadSeam();
    },

    loadSeam: function(){
        baseballSpin3D.loadingSeam = true;
        new JSONLoader().load(baseballJSON['seam']);
    },

    finalizeSeam: function(geometry){
        let material = new THREE.MeshLambertMaterial({
            color: new THREE.Color(0x767373),
            side: THREE.DoubleSide,
            transparent: false,
            reflectivity:0.1
        });
        let mesh = new THREE.Mesh(geometry, material);
        mesh.overdraw = true;
        console.log(mesh);
        baseballSpin3D.baseballGroup.add(mesh);
        mesh.position.copy(baseballSpin3D.position);
        baseballSpin3D.loadingSeam = false;
        baseballSpin3D.arrowHelpers();
    },

    arrowHelpers: function(){
        let dirPos = new THREE.Vector3( 1, 0, 0 );
        let dirNeg = new THREE.Vector3( -1, 0, 0 );

        //normalize the direction vector (convert to vector of length 1)
        //dirPos.normalize();
        //dirNeg.normalize();

        let origin = new THREE.Vector3( 0, 0, 0 );
        let length = 1;
        //let hex = '#00cc00';
        let hex = 0x00cc00;

        let arrowHelperPos = new THREE.ArrowHelper( dirPos, origin, length, hex );
        let arrowHelperNeg = new THREE.ArrowHelper( dirNeg, origin, length, hex );
        baseballSpin3D.baseballGroup.add( arrowHelperPos );
        baseballSpin3D.baseballGroup.add( arrowHelperNeg );
        baseballSpin3D.finalize();
    },

    /*
    loadGeometry: function(jsonObj) {
        return new Promise(resolve => {
            new JSONLoader().load(jsonObj, resolve);
        });
    },

    processObjects: function(jsonObj, material){

        const promises = [
            baseballSpin3D.loadGeometry(jsonObj)
        ];

        return Promise.all(promises).then(result => {
            return new THREE.Mesh(result[0], material);
        });
    },
    */

    computeVertices: function(geometry) {
        geometry.mergeVertices();
        geometry.computeVertexNormals(true);
        geometry.computeFaceNormals();
        geometry.computeBoundingBox();
        let max = geometry.boundingBox.max,
            min = geometry.boundingBox.min;
        let offset = new THREE.Vector2(0 - min.x, 0 - min.y),
            range = new THREE.Vector2(max.x - min.x, max.y - min.y),
            faces = geometry.faces;

        geometry.faceVertexUvs[0] = [];

        for (let j = 0; j < faces.length; j++) {

            let v1 = geometry.vertices[faces[j].a],
                v2 = geometry.vertices[faces[j].b],
                v3 = geometry.vertices[faces[j].c];

            geometry.faceVertexUvs[0].push([
                new THREE.Vector2((v1.x + offset.x) / range.x, (v1.y + offset.y) / range.y),
                new THREE.Vector2((v2.x + offset.x) / range.x, (v2.y + offset.y) / range.y),
                new THREE.Vector2((v3.x + offset.x) / range.x, (v3.y + offset.y) / range.y)
            ]);
        }
        geometry.uvsNeedUpdate = true;
    },

    /*
    addBaseballWrapped: function(){
        const loader = new THREE.TextureLoader();
        loader.load( 'textures/baseball_diffuse.jpg', function ( texture ) {

            texture.anisotropy = baseballSpin3D.renderer.capabilities.getMaxAnisotropy();
            let  repeatX, repeatY;
            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            texture.mapping = THREE.SphericalReflectionMapping;
            texture.magFilter = THREE.NearestFilter;
            texture.minFilter = THREE.NearestFilter;
            //let ballDiaPixels = 278.4;
            //let textureWidth = 1600;
            //let textureHeight = 410;
            //repeatX = ballDiaPixels * textureHeight / (ballDiaPixels * textureWidth);
            //repeatY = 1;
            //texture.repeat.set(repeatX, repeatY);
            //texture.offset.x = (repeatX - 1) / 2 * -1;
            texture.needsUpdate = true;

            let geometry = new THREE.SphereGeometry( 0.2416666666666667, 100, 100 );
            baseballSpin3D.computeVertices(geometry);

            let material = new THREE.MeshLambertMaterial( {
                //color: new THREE.Color(0xC10E0E),
                map: texture,
                overdraw: true,
                castShadow: true,
                receiveShadow: true,
                side: THREE.DoubleSide,
                transparent: false,
                reflectivity:0.1
            } );
            let baseball = new THREE.Mesh( geometry, material );
            baseball.position.copy(baseballSpin3D.position);
            baseballSpin3D.baseballGroup.add( baseball );

            baseballSpin3D.finalize();
        } );
    },
    */

    toRadians: function(degrees) {
        return degrees * Math.PI / 180;
    },


    rotateBaseball: function(degreesX, degreesY, degreesZ, resetFirst){
        let deltaRotationQuaternion = new THREE.Quaternion()
            .setFromEuler(new THREE.Euler(
                baseballSpin3D.toRadians(degreesX * -1),
                baseballSpin3D.toRadians(degreesY * 1),
                baseballSpin3D.toRadians(degreesZ * 1),
                'XYZ'
            ));

        for (let i = 0; i < baseballSpin3D.baseballObjects.length; i++) {
            let object = baseballSpin3D.baseballObjects[i];

            if(resetFirst){
                object.lookAt(baseballSpin3D.defaultVector);
            }

            object.quaternion.multiplyQuaternions(deltaRotationQuaternion, object.quaternion);
        }
    },

    pitchTypeSet: function(pitchType){
        switch(pitchType){
            case "FA":
                baseballSpin3D.rotateBaseball(0,0,0,true);
                baseballSpin3D.currentRotationVector = baseballSpin3D.pitchAxes["FA"];
                baseballSpin3D.currentRotationDirection = baseballSpin3D.pitchSpeedDirection["FA"];
                break;
            case "FT":
                baseballSpin3D.rotateBaseball(0,90,0,true);
                baseballSpin3D.currentRotationVector = baseballSpin3D.pitchAxes["FT"];
                baseballSpin3D.currentRotationDirection = baseballSpin3D.pitchSpeedDirection["FT"];
                break;

        }
    },

    finalize: function(){

        /*
        let geometry2 = new THREE.SphereGeometry( 1, 16, 16 );
        let material2 = new THREE.MeshBasicMaterial( {color: new THREE.Color(0x00ff40)} );
        let sphere2 = new THREE.Mesh( geometry2, material2 );
        sphere2.baseballSpin3D.position.copy(baseballSpin3D.position);
        sphere2.renderOrder = 1000;
        sphere2.onBeforeRender = function (renderer) {
            renderer.clearDepth();
        };
        baseballSpin3D.baseballGroup.add( sphere2 );
        */

        baseballSpin3D.scene.add(baseballSpin3D.baseballGroup);
        baseballSpin3D.baseballObjects = baseballSpin3D.baseballGroup.children;

        // four-seam fastball default


        // two-seam fastball
        baseballSpin3D.rotateBaseball(0,90,0,true);



        let axesHelper = new THREE.AxesHelper( 150 );
        baseballSpin3D.scene.add( axesHelper );

        // controls
        baseballSpin3D.controls = new THREE.OrbitControls(baseballSpin3D.turnTableCamera, baseballSpin3D.renderer.domElement);
        baseballSpin3D.controls.target = baseballSpin3D.position;
        baseballSpin3D.controls.enabled = false;
        //baseballSpin3D.controls.enabled = true;
        baseballSpin3D.turnTableCamera.position.x = 0;
        baseballSpin3D.turnTableCamera.position.y = 100;
        baseballSpin3D.turnTableCamera.position.z = 0;
        baseballSpin3D.turnTableCamera.lookAt(baseballSpin3D.position);
        baseballSpin3D.turnTableCamera.updateMatrixWorld();
        baseballSpin3D.turnTableCamera.updateProjectionMatrix();
        baseballSpin3D.controls.update();

        $(baseballSpin3D.renderer.domElement).on('mousedown', function(e) {
            baseballSpin3D.isDragging = true;
        })
            .on('mousemove', function(e) {
                //console.log(e);
                let deltaMove = {
                    x: e.offsetX-baseballSpin3D.previousMousePosition.x,
                    y: e.offsetY-baseballSpin3D.previousMousePosition.y
                };

                if(baseballSpin3D.isDragging) {

                    let deltaRotationQuaternion = new THREE.Quaternion()
                        .setFromEuler(new THREE.Euler(
                            baseballSpin3D.toRadians(deltaMove.y * -.5),
                            0,
                            baseballSpin3D.toRadians(deltaMove.x * .5),
                            'YXZ'
                        ));

                    let objects = baseballSpin3D.baseballGroup.children;
                    for (let i = 0; i < objects.length; i++) {
                        let object = objects[i];

                        object.quaternion.multiplyQuaternions(deltaRotationQuaternion, object.quaternion);
                    }
                }

                baseballSpin3D.previousMousePosition = {
                    x: e.offsetX,
                    y: e.offsetY
                };
            });
        /* */

        $(document).on('mouseup', function(e) {
            baseballSpin3D.turnTableCamera.updateMatrixWorld();
            baseballSpin3D.turnTableCamera.updateProjectionMatrix();
            baseballSpin3D.isDragging = false;
        });


        baseballSpin3D.startAnimation();
    },

    startAnimation: function() {
        baseballSpin3D.animationFrame = requestAnimationFrame(baseballSpin3D.startAnimation);
        baseballSpin3D.render();
    },

    render: function() {
        baseballSpin3D.renderer.render(baseballSpin3D.scene, baseballSpin3D.turnTableCamera);

        if(!baseballSpin3D.isDragging) {
            for (let i = 0; i < baseballSpin3D.baseballObjects.length; i++) {
                let object = baseballSpin3D.baseballObjects[i];
                object.rotateOnWorldAxis(baseballSpin3D.currentRotationVector[0],  baseballSpin3D.currentRotationDirection[0]);
                if (baseballSpin3D.currentRotationVector[1]) {
                    object.rotateOnWorldAxis(baseballSpin3D.currentRotationVector[1], baseballSpin3D.currentRotationDirection[1])
                }
            }
        }

    }
};